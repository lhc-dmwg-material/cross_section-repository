Responsibles :
N.N. <n.n@cern.ch> - ATLAS
N.N. <n.n@cern.ch> - CMS
N.N. <n.n@cern.ch> - Theory

This folder stores the information relevant to the definition of benchmark
points for a scan of major parameters of the 2HDM+a model
(https://arxiv.org/abs/1701.07427, 
UFO https://gitlab.cern.ch/lhc-dmwg-material/model-repository/tree/master/models/Pseudoscalar_2HDM),
for the Higgs + MET signature. The major parameters of the scan are kinematic
parameters: mA and ma, which notably change the yield and dsigma/dMET. Also
relevant are mH and mH+, tan(beta), and sin(theta) parameters, however, with a smaller
effect on dsigma/dMET. Other parameters are m_DM, and y_DM. Some of
the preliminary documentation for the parameter scan definition:
  https://indico.cern.ch/event/632340/timetable/
  https://indico.cern.ch/event/646857/

The motivation (2HDM vacuum stability) to change the proposal is discussed here:
  https://indico.cern.ch/event/646857/contributions/2634493/attachments/1479924/2294462/DMLHC_No_June.pdf
The proposal is documented at:
  <to be filled>

To ensure coherency, reference cross-sections for a wide range of benchmark
points are provided.

CrossSections_ATLAS.txt
==> This file contains reference cross-sections for a wide range of benchmark
    points

For each of the 105 parameter points, 5,000 MC events were generated.
The resulting cross-sections and uncertainties are listed in CrossSections_ATLAS.txt.
For each of the 105 parameter points, the  param_card, run_card and proc card are provided in cards_ATLAS.
The cards in cards_ATLAS are named corresponding to the model parameters and initial state particles:
<ggF/bb>_tb<tanbeta>_sp<sintheta>_mA<mA>_ma<ma>.<param/run/MG5_proc>_card.dat if the Dark Matter mass IS the default of 10 GeV.
<ggF/bb>_tb<tanbeta>_sp<sintheta>_mA<mA>_ma<ma>_mDM<DM mass>.<param/run/MG5_proc>_card.dat if the Dark Matter mass IS NOT the defualt of 10 Gev.


The cards for a previous iteration of benchmark points are stored in the  './initial_scan_ATLAS' 
directory

